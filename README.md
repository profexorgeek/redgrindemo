# README #
This project demonstrates the implementation of the RedGrin lidgren wrapper. This demo utilizes 
FlatRedBall, a working knowledge of FlatRedBall and FRBDK will be helpful to understand how this
demo works!

RedGrin can be found here:
https://bitbucket.org/profexorgeek/redgrin

## How do I get set up? ##

* Ensure you have pulled the RedGrin library and it exists in the same parent folder as this project
* OPTIONAL: Open the csproj with the FRBDK tool "Glue" to regenerate boilerplate code
* Run two instances of the RedGrinTest project
* Press CTRL+S to start a server in instance 1
* Press CTRL+C to start a client in instance 2
* Press Space to create an entity
* Press WASD to move  entity around on screen
* Press CTRL+K to kill entity
* Both instances should stay in sync

_Note: Depending on your development environment, you may need to install FlatRedBall to build this project. Find instructions at http://flatredball.com_

## How does it work? ##

### The Lobby game screen ###
The Lobby screen (found in the Screens folder) implements INetworkArena, establishing a contract with Redgrin to provide several methods. These methods 
create and destroy entities and bind some basic user input to RedGrin connect methods. Everything else is handled by RedGrin! 
The network is initialized once in the CustomInitialize() method, which calls InitializeNetwork(). Pay special attention to the
config object that is created in this method. It must enumerate each unique type of network message. You can also experiment
with the simulated latency and loss properties in this object to simulate bad connections.

Next DoNetworking() is called during every tick of the game loop. DoNetworking() tells the Redgrin network object to update itself. 
Then it checks if it's time to perform a "dead reckoning" to ensure all clients are in sync. If so a dead reckoning message will 
be sent to all clients.

With Redgrin, you do not directly instantiate networked game entities. Instead, you ask Redgrin to create an entity and it performs
specific logic depending on if the instance is a server or a client! Ultimately it will call RequestCreateEntity and RequestDestroyEntity
to create and destroy game objects on the INetworkArena. You will notice that our Lobby screen has implementations of both of these methods.

All networked entities will automatically be kept up to date by Redgrin as long as they are created and destroyed properly! Only entities that
must stay in sync across a network need to be created this way. Objects that do not influence gameplay, such as particle effects, decorative objects
and similar game entities do not need to be managed by Redgrin.


### The NetworkPlayerModel ###
The NetworkPlayerModel (found in the NetworkModels folder) is a data structure that represents a player entity as an object that can be serialized and transferred through a 
network connection. In this demo we only have a single type of networked entity. In a real game you would likely have a message type for
every unique entity type. When the INetworkArena receives a CreateEntity request, it will include a model that it should use to instantiate
the new entity!

Models are serialized by Redgrin using reflection. Models should always contain the minimum amount of data required to keep entities in sync
across a network. In this case, we only care about the X and Y position, the velocity and the Rotation of our object. Velocity could be used by the
entity for basic predictive physics, which make gameplay appear smoother. If our game entity did not move or only moved rarely, we would not
need to transmit the velocity!

### The NetworkPlayser ###
The NetworkPlayer (found in the Entities folder) is our game entity. It implements INetworkEntity, allowing Redgrin to track this entity, the client
that first created it, and get and set the entity state.

Typically a client should have a lot of influence over the state of entities that it owns. When UpdateState is called on this entity, Redgrin will pass
a network model which we will want to apply to this game object. However, we don't want to always just accept that model. So, our implementation does not apply
the model if this game object is our local player, this client should be the master record of its own player state! However, during a dead reckoning the server
needs to force all clients to be in sync. Thus if this is a dead reckoning message we allow the network state to be applied.

Notice that SetFromModel and GetState simply convert between the actual game entity and the representative state that is transferred over the network.

Finally, notice that our ApplyInput() method, which is called every tick of the game loop, checks to see if our rotation or velocity have changed. It only
requests an entity update if these values have changed. We don't want to hammer the network with identical status updates every single frame, this will
quickly degrade our game performance with just a few entities. Instead, we only send updates when our entity has made a change in velocity or rotation.

### Next Step Ideas ###
This is the absolute most basic implementation of a network game, simply demonstrating the usage of Redgrin. Some ideas for next steps:

* Add other entity types and corresponding NetworkModels
* Implement entity collision (this will have interesting consequences for how movement is applied!)
* Interpolate between network state and local state for smoother reckoning
* Network config shouldn't be hardcoded in the screen
* Allow users to specify connection address (currently hardcoded to localhost)

## Who should I talk to? ##
https://twitter.com/profexorgeek
