using System;
using System.Collections.Generic;
using System.Text;
using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using RedGrin;
using RedgrinTest.NetworkModels;
using RedgrinTest.Input;
using Microsoft.Xna.Framework;

namespace RedgrinTest.Entities
{
	public partial class NetworkPlayer : INetworkEntity
	{
        public long OwnerId { get; set; }
        public long EntityId { get; set; }
        public IPlayerInput Controller { get; set; }
        public bool IsLocalPlayer { get; set; }
        public float lastRotation;
        public Vector3 lastVelocity;

        private void CustomInitialize()
		{


		}

		private void CustomActivity()
		{
            ApplyInput();
		}

		private void CustomDestroy()
		{


		}

        private static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        public void ApplyInput()
        {
            if(Controller != null)
            {
                if(Controller.Steering.Magnitude > 0.2f)
                {
                    RotationZ = Controller.Steering.GetAngle().Value;
                    Velocity = RotationMatrix.Right * 100;
                }
                else
                {
                    Velocity = Vector3.Zero;
                }
            }

            // send update if we own this entity and its state has changed
            if(OwnerId == Game1.Network.NetworkId)
            {
                if (RotationZ != lastRotation || Velocity != lastVelocity)
                {
                    Game1.Network.RequestUpdateEntity(this);
                }
            }

            lastRotation = RotationZ;
            lastVelocity = Velocity;
        }

        public void SetFromModel(NetworkPlayerModel model)
        {
            this.X = model.X;
            this.Y = model.Y;
            this.Velocity.X = model.VelocityX;
            this.Velocity.Y = model.VelocityY;
            this.RotationZ = model.Rotation;
        }

        public object GetState()
        {
            return new NetworkPlayerModel()
            {
                X = this.X,
                Y = this.Y,
                VelocityX = this.Velocity.X,
                VelocityY = this.Velocity.Y,
                Rotation = this.RotationZ
            };
        }

        public void UpdateState(object entityState, double stateTime, bool isReckoningState = false)
        {
            if(isReckoningState || !IsLocalPlayer)
            {
                SetFromModel((NetworkPlayerModel)entityState);
            }
        }
    }
}
