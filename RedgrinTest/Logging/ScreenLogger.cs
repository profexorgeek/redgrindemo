﻿using RedGrin.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlatRedBall;

namespace RedgrinTest.Logging
{

    public class ScreenLogger : ILogger
    {
        private static ScreenLogger instance;

        public LogLevels Level { get; set; }

        public static ScreenLogger Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new ScreenLogger();
                }

                return instance;
            }
        }

        #region ILogger
        public void Debug(string message)
        {
            Write(LogLevels.Debug, message);
        }

        public void Info(string message)
        {
            Write(LogLevels.Info, message);
        }

        public void Warning(string message)
        {
            Write(LogLevels.Warning, message);
        }

        public void Error(string message)
        {
            Write(LogLevels.Error, message);
        }
        #endregion

        private void Write(LogLevels level, string message)
        {
            // TODO: this should respect the defined log level
            FlatRedBall.Debugging.Debugger.CommandLineWrite(level.ToString() + ": " + message);
        }
    }
}
