﻿using FlatRedBall.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedgrinTest.Input
{
    public interface IPlayerInput
    {
        I2DInput Steering { get; }
    }
}
