﻿using FlatRedBall.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RedgrinTest.Input
{
    public class KeyboardInput : IPlayerInput
    {
        private I2DInput steering;

        public I2DInput Steering
        {
            get
            {
                return steering;
            }
        }


        public KeyboardInput()
        {
            steering = InputManager.Keyboard.Get2DInput(
                Microsoft.Xna.Framework.Input.Keys.A, 
                Microsoft.Xna.Framework.Input.Keys.D, 
                Microsoft.Xna.Framework.Input.Keys.W, 
                Microsoft.Xna.Framework.Input.Keys.S);
        }
    }
}
