using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using FlatRedBall;
using FlatRedBall.Input;
using FlatRedBall.Instructions;
using FlatRedBall.AI.Pathfinding;
using FlatRedBall.Graphics.Animation;
using FlatRedBall.Graphics.Particle;
using FlatRedBall.Math.Geometry;
using FlatRedBall.Localization;
using RedGrin;
using RedgrinTest.Logging;
using RedgrinTest.NetworkModels;
using RedgrinTest.Factories;
using RedgrinTest.Entities;
using RedgrinTest.Input;

namespace RedgrinTest.Screens
{
	public partial class Lobby : INetworkArena
	{
        private NetworkConfiguration config;
        private float timeToDeadReckon = 0;

        public NetworkPlayer LocalPlayer { get; set; }


		void CustomInitialize()
		{
            InitializeNetwork();
        }

		void CustomActivity(bool firstTimeCalled)
		{
            DoUserInput();
            DoNetworking();
		}

		void CustomDestroy()
		{
            Game1.Network.Disconnect();

		}

        static void CustomLoadStaticContent(string contentManagerName)
        {


        }

        private void DoNetworking()
        {
            if (Game1.Network != null)
            {
                Game1.Network.Update();

                if(Game1.Network.Role == NetworkRole.Server)
                {
                    if (timeToDeadReckon <= 0)
                    {
                        Game1.Network.DeadReckon();
                        timeToDeadReckon = 3f;
                        ScreenLogger.Instance.Info("Performing dead reckon.");
                    }


                    timeToDeadReckon -= TimeManager.SecondDifference;
                }
            }
        }

        private void InitializeNetwork()
        {
            config = new NetworkConfiguration()
            {
                ApplicationName = "RedgrinTest",
                ApplicationPort = 1337,
                SimulatedDuplicateChance = 0,
                SimulatedLoss = 0,
                SimulatedMinimumLatencySeconds = 0,
                SimulatedRandomLatencySeconds = 0,
                EntityStateTypes = new List<Type>()
                {
                    typeof(NetworkPlayerModel)
                }
             };

            Game1.Network = new NetworkManager(config, ScreenLogger.Instance);
            Game1.Network.GameArena = this;
        }

        private void DoUserInput()
        {
            if(InputManager.Keyboard.KeyDown(Microsoft.Xna.Framework.Input.Keys.LeftControl))
            {
                if (InputManager.Keyboard.KeyPushed(Microsoft.Xna.Framework.Input.Keys.C))
                {
                    Game1.Network.Initialize(NetworkRole.Client);
                    Game1.Network.Connect("127.0.0.1");
                }

                if (InputManager.Keyboard.KeyPushed(Microsoft.Xna.Framework.Input.Keys.S))
                {
                    Game1.Network.Initialize(NetworkRole.Server);
                }

                if(InputManager.Keyboard.KeyPushed(Microsoft.Xna.Framework.Input.Keys.K))
                {
                    if(LocalPlayer != null)
                    {
                        Game1.Network.RequestDestroyEntity(LocalPlayer);
                    }
                }
            }

            if(InputManager.Keyboard.KeyPushed(Microsoft.Xna.Framework.Input.Keys.Space) && LocalPlayer == null)
            {
                var e = new NetworkPlayerModel()
                {
                    X = 100,
                    Y = 100,
                    VelocityX = 0,
                    VelocityY = 0
                };

                Game1.Network.RequestCreateEntity(e);
            }
        }


        #region INetworkArena
        public INetworkEntity RequestCreateEntity(long ownerId, long entityId, object entityData)
        {
            INetworkEntity created = null;

            if(entityData.GetType() == typeof(NetworkPlayerModel))
            {
                var e = NetworkPlayerFactory.CreateNew();
                e.SetFromModel((NetworkPlayerModel)entityData);

                // if the owner is this machine, it is the local player
                if (ownerId == Game1.Network.NetworkId)
                {
                    LocalPlayer = e;
                    LocalPlayer.Controller = new KeyboardInput();
                    LocalPlayer.IsLocalPlayer = true;
                }

                created = e;
            }

            return created;
        }

        public void RequestDestroyEntity(INetworkEntity entity)
        {
            if(entity is NetworkPlayer)
            {
                ((NetworkPlayer)entity).Destroy();

                if(entity == LocalPlayer)
                {
                    LocalPlayer = null;
                }
            }
        }
        #endregion
    }
}
